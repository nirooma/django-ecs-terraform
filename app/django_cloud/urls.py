from django.contrib import admin
from django.urls import path
from cloud.views import check_helth, check_login

urlpatterns = [
    path('admin/', admin.site.urls),
    path('check_helth/', check_helth),
    path('check_login/', check_login)
]
